package se325.lab02.concert.services;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

import javax.ws.rs.*;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;

import se325.lab02.concert.common.Config;
import se325.lab02.concert.domain.Concert;

import org.apache.commons.lang3.NotImplementedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class to implement a simple REST Web service for managing Concerts.
 * <p>
 * ConcertResource implements a WEB service with the following interface:
 * - GET    <base-uri>/concerts/{id}
 * Retrieves a Concert based on its unique id. The HTTP response
 * message has a status code of either 200 or 404, depending on
 * whether the specified Concert is found.
 * <p>
 * - GET    <base-uri>/concerts?start&size
 * Retrieves a collection of Concerts, where the "start" query
 * parameter identifies an index position, and "size" represents the
 * max number of successive Concerts to return. The HTTP response
 * message returns 200.
 * <p>
 * - POST   <base-uri>/concerts
 * Creates a new Concert. The HTTP post message contains a
 * representation of the Concert to be created. The HTTP Response
 * message returns a Location header with the URI of the new Concert
 * and a status code of 201.
 * <p>
 * - DELETE <base-uri>/concerts
 * Deletes all Concerts, returning a status code of 204.
 * <p>
 * Where a HTTP request message doesn't contain a cookie named clientId
 * (Config.CLIENT_COOKIE), the Web service generates a new cookie, whose value
 * is a randomly generated UUID. The Web service returns the new cookie as part
 * of the HTTP response message.
 */
@Path("/concerts")
public class ConcertResource {

    private static Logger LOGGER = LoggerFactory.getLogger(ConcertResource.class);

    private Map<Long, Concert> concertDB = new ConcurrentHashMap<>();
    private AtomicLong idCounter = new AtomicLong();


    @Path("{id}")
    @GET
    @Produces("application/java-serialization")
    public Response retrieveConcert(@PathParam("id") long id, @CookieParam("clientId") Cookie clientId) {
        Concert concert = concertDB.get(id);
        if (concert == null) {
            ResponseBuilder builder = Response.status(404);
            return builder.build();
        }

        ResponseBuilder builder = Response.ok(concert);
        if (clientId == null) {
            builder = builder.cookie(makeCookie(null));
        }
        return builder.build();
    }

    @GET
    @Produces("application/java-serialization")
    public Response retrieveConcerts(@QueryParam("start") long start, @QueryParam("size") int size, @CookieParam("clientId") Cookie clientId) {
        // The Response object should store an ArrayList<Concert> entity. The
        // ArrayList can be empty depending on the start and size arguments,
        // and Concerts stored.
        //
        // Because of type erasure with Java Generics, any generically typed
        // entity needs to be wrapped by a javax.ws.rs.core.GenericEntity that
        // stores the generic type information. Hence to add an ArrayList as a
        // Response object's entity, you should use the following code:

        int skip = 1;
        List<Concert> concerts = new ArrayList<Concert>();
        for (Map.Entry<Long, Concert> entry : concertDB.entrySet()) {
            if (skip < start) {
                skip++;
            }
            else{
                if (concerts.size() < size){
                    concerts.add(entry.getValue());
                }
            }
//            if ((entry.getValue().getId() >= start) & (concerts.size() < size)) {
//                concerts.add(entry.getValue());
//            }
        }
        GenericEntity<List<Concert>> entity = new GenericEntity<List<Concert>>(concerts) {
        };
        ResponseBuilder builder = Response.ok(entity);
        if (clientId == null) {
            builder = builder.cookie(makeCookie(null));
        }
        return builder.build();
    }

    @POST
    @Consumes("application/java-serialization")
    public Response createConcert(Concert concert, @CookieParam("clientId") Cookie clientId) {
        // Reminder: You won't need to annotate the "concert" argument above - 
        // arguments without annotations are assumed by JAX-RS to come from the HTTP request body.

        long id = idCounter.getAndIncrement();
        Concert newConcert = new Concert(id, concert.getTitle(), concert.getDate());
        concertDB.put(id, newConcert);

        ResponseBuilder builder = Response.created(URI.create("/concerts/" + newConcert.getId())); //response.ok(concert));
        if (clientId == null) {
            builder.cookie(makeCookie(null));
        }

//        concertDB.put(concert.getId(), concert);
//        ResponseBuilder builder = Response.created(URI.create("/concerts/" + concert.getId())); //response.ok(concert));
//        if (clientId == null) {
//            builder.cookie(makeCookie(null));
//        }

        return builder.build();
    }


    @DELETE
    public Response deleteAllConcerts(@CookieParam("clientId") Cookie clientId) {
        concertDB = new ConcurrentHashMap<>();
        ResponseBuilder builder = Response.status(204);
        if (clientId == null) {
            builder = builder.cookie(makeCookie(null));
        }
        return builder.build();
    }

    /**
     * Helper method that can be called from every service method to generate a
     * NewCookie instance, if necessary, based on the clientId parameter.
     *
     * @param clientId the Cookie whose name is Config.CLIENT_COOKIE, extracted
     *                 from a HTTP request message. This can be null if there was no cookie
     *                 named Config.CLIENT_COOKIE present in the HTTP request message.
     * @return a NewCookie object, with a generated UUID value, if the clientId
     * parameter is null. If the clientId parameter is non-null (i.e. the HTTP
     * request message contained a cookie named Config.CLIENT_COOKIE), this
     * method returns null as there's no need to return a NewCookie in the HTTP
     * response message.
     */
    private NewCookie makeCookie(Cookie clientId) {
        NewCookie newCookie = null;

        if (clientId == null) {
            newCookie = new NewCookie(Config.CLIENT_COOKIE, UUID.randomUUID().toString());
            LOGGER.info("Generated cookie: " + newCookie.getValue());
        }

        return newCookie;
    }
}
