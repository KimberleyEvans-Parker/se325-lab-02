package se325.lab02.parolee.test;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Simple JUnit test to test the behaviour of the Parolee Web service.
 * <p>
 * The test is implemented using the JAX-RS client API.
 */
public class ParoleeResourceIT {

    private static final String PAROLEE_JSON_TEMPLATE =
            "{ \"firstName\": \"%s\", \"lastName\": \"%s\", \"gender\": \"%s\", \"dateOfBirth\": \"%s\" }";

    private static Logger _logger = LoggerFactory
            .getLogger(ParoleeResourceIT.class);

    private static String WEB_SERVICE_URI = "http://localhost:10000/services/parolees";

    private static Client client;

    private static String[] JSON_PAYLOADS = {
            String.format(PAROLEE_JSON_TEMPLATE, "Al", "Capone", "MALE", "1899-01-17"),
            String.format(PAROLEE_JSON_TEMPLATE, "John", "Gotti", "MALE", "1940-10-27"),
            String.format(PAROLEE_JSON_TEMPLATE, "Pablo", "Escobar", "MALE", "1949-12-01"),
            String.format(PAROLEE_JSON_TEMPLATE, "Carlos", "Marcello", "MALE", "1910-02-06")
    };

    private static List<String> paroleeUris = new ArrayList<>();


    @BeforeClass
    public static void createClient() {
        // Use ClientBuilder to create a new client that can be used to create
        // connections to the Web service.
        client = ClientBuilder.newClient();
    }

    @AfterClass
    public static void closeConnection() {
        client.close();
    }

    @Before
    public void clearAndPopulate() {
        // Delete all Parolees in the Web service.
        Response response = client.target(WEB_SERVICE_URI).request().delete();
        response.close();

        // Clear Parolee Uris
        paroleeUris.clear();

        // Populate the service with Parolees.
        for (String payload : JSON_PAYLOADS) {
            response = client.target(WEB_SERVICE_URI).request().post(Entity.json(payload));
            String paroleeUri = response.getLocation().toString();
            paroleeUris.add(paroleeUri);
            response.close();
        }
    }

    @Test
    public void testCreate() {
        // JSON representation of the new Parolee.
        String jsonPayload = String.format(PAROLEE_JSON_TEMPLATE, "Jesse", "James", "MALE", "1847-09-05");

        // Make a HTTP POST request to create a new Parolee.
        Response response = client.target(WEB_SERVICE_URI).request().post(Entity.json(jsonPayload));

        // Check that the HTTP response code is 201 Created.
        int responseCode = response.getStatus();
        assertEquals(201, responseCode);

        String paroleeUri = response.getLocation().toString();
        _logger.info("Uri of newly created Parolee: " + paroleeUri);

        // Close the Response object.
        response.close();
    }

    @Test
    public void testRetrieve() throws IOException {
        String paroleeUri = paroleeUris.get(paroleeUris.size() - 1);

        // Make a HTTP GET request to retrieve the last created Parolee.
        Response response = client.target(paroleeUri).request().get();

        // Check that the HTTP response code is 200 OK.
        int responseCode = response.getStatus();
        assertEquals(200, responseCode);

        String jsonResponse = response.readEntity(String.class);
        _logger.info("Retrieved Parolee JSON: " + jsonResponse);

        // Close the Response object.
        response.close();

    }

    @Test
    public void testUpdate() {
        // Create a XML representation of the first parolee, changing Al
        // Capone's gender.
        String updateParolee = String.format(PAROLEE_JSON_TEMPLATE, "Al", "Capone", "FEMALE", "1899-01-17");

        // Make a HTTP PUT request to update the Parolee.
        Response response = client.target(WEB_SERVICE_URI + "/1").request().put(Entity.json(updateParolee));

        // Check that the HTTP response code is 204 No content.
        int status = response.getStatus();
        assertEquals(204, status);

        // Close the Response object.
        response.close();
    }

    @Test
    public void testDelete() {
        // Make a HTTP DELETE request to delete the first Parolee.
        Response response = client.target(WEB_SERVICE_URI + "/1").request().delete();

        // Check that the HTTP response code is 204 No content.
        int status = response.getStatus();
        assertEquals(204, status);

        // Close the Response object.
        response.close();
    }
}