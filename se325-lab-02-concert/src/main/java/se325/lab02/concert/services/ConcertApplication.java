package se325.lab02.concert.services;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/services")
public class ConcertApplication extends Application {
	
	private final Set<Object> singletons = new HashSet<>(); 
	private Set<Class<?>> classes = new HashSet<Class<?>>();
	
	public ConcertApplication() {
	  classes.add(SerializationMessageBodyReaderAndWriter.class);
	  singletons.add(new ConcertResource());
	}
	
	@Override
	public Set<Class<?>> getClasses() {
	  return classes;
	}
	
	@Override public Set<Object> getSingletons() { 
		return singletons;
	}

}
